﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sourcesDrains
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int[,] matrix = new int[n, n];
            for (int i = 0; i < n; i++)
            {
                string s = Console.ReadLine();
                string[] s_arr = s.Split(' ');

                for (int j = 0; j < n; j++)
                {
                    matrix[i, j] = int.Parse(s_arr[j]);
                }
            }
            int countFree = 0, count = 0;
            string ind = "",str = "";
            int index = 0;
            for (int i = 0; i < n; i++)
            {
                countFree = 0;
                for (int j = 0; j < n; j++)
                {
                    if (matrix[j, i] == 0) {
                        countFree++;
                    }
                }
                if(countFree == n)
                {
                    index = i;
                    count++;
                    str+= " " + (index+1);
                }
            }
            ind += count + str;
            Console.Write(ind);
            Console.WriteLine();

            index = 0;
            countFree = 0;
            count = 0;
            ind = "";
            str ="";
            for (int i = 0; i < n; i++)
            {
                countFree = 0;
                for (int j = 0; j < n; j++)
                {
                    if (matrix[i, j] == 0)
                    {
                        countFree++;
                        
                    }
                }
                if (countFree == n)
                {
                    index = i;
                    count++;
                    str += " " + (index + 1);
                }
            }
            ind += count + str;
            Console.Write(ind);
            Console.WriteLine();
            Console.ReadKey();
        }
    }
}
