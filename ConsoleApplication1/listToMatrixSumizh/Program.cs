﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace listToMatrixSumizh
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int n = int.Parse(Console.ReadLine());
                int[,] matrix = new int[n, n];

                for (int i = 0; i < n; i++)
                {
                    string s = Console.ReadLine();
                    string[] s_arr = s.Split(' ');
                    int countR = int.Parse(s_arr[0]);
                    for (int j = 1; j <= countR; j++)
                    {
                        matrix[i, int.Parse(s_arr[j])-1] = 1;
                    }
                }

                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j < n; j++)
                    {
                        Console.Write(matrix[i, j] + " ");
                    }
                    Console.WriteLine();
                }
            }
            catch(Exception ex)
            {
                Console.Write(ex.Message);
            }
           
            Console.ReadKey();
        }
    }
}
