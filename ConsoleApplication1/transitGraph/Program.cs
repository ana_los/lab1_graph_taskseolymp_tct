﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace transitGraph
{
    class Program
    {

        static bool Transit(int [,]matrix, bool flag)
        {
            /*for (int i = 0; i < matrix.GetLength(0); i++)
            {
                if (matrix[i, i] == 1)
                {
                    flag = false; return flag;
                }
            }*/

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] == 1)
                    {
                        for (int k = 0; k < matrix.GetLength(0); k++)
                        {
                           if (k != i && matrix[j, k] == 1 && matrix[i,k] != 1) { flag = false; return flag; }
                        }
                    }
                }
            }
            return flag;
        }
        static void Main(string[] args)
        {
            string s = Console.ReadLine();
            string[] s_arr = s.Split(' ');
            int n = int.Parse(s_arr[0]);
            int m = int.Parse(s_arr[1]);

            int[,] matrix = new int[n, n];
            for (int i = 0; i < m; i++)
            {
                s = Console.ReadLine();
                s_arr = s.Split(' ');
                matrix[int.Parse(s_arr[0]) - 1, int.Parse(s_arr[1]) - 1] = 1;
                matrix[int.Parse(s_arr[1]) - 1, int.Parse(s_arr[0]) - 1] = 1;
            }

            bool flag = true;

            if (Transit(matrix,flag) == true)
            {
                Console.Write("YES");
            }
            else Console.Write("NO");

            Console.ReadKey();
        }
    }
}
