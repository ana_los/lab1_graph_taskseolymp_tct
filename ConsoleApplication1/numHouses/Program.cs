﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace numHouses
{
    class Program
    {
        static void Main(string[] args)
        {
            string s = Console.ReadLine();
            string[] s_arr = s.Split(' ');
            int n = int.Parse(s_arr[0]);
            int m = int.Parse(s_arr[1]);

            if(n % 2 == 0 && m % 2 == 0)
            {
                Console.Write(1);
            }
            else Console.Write(0);

            Console.ReadKey();

        }
    }
}
