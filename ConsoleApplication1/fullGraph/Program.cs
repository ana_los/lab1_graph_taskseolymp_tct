﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fullGraph
{
    class Program
    {
        static void Main(string[] args)
        {
            string s = Console.ReadLine();
            string[] s_arr = s.Split(' ');
            int n = int.Parse(s_arr[0]);//V
            int m = int.Parse(s_arr[1]);//R

            int[,] matrix = new int[n,n];
            for(int i = 0; i < m; i++)
            {
                s = Console.ReadLine();
                s_arr = s.Split(' ');
                matrix[int.Parse(s_arr[0]) - 1, int.Parse(s_arr[1]) - 1] = 1;
                matrix[int.Parse(s_arr[1]) - 1, int.Parse(s_arr[0]) - 1] = 1;
            }
            bool flag = true;
            for(int i = 0; i < n; i++)
            {
                for(int j = 0; j < n; j++)
                {
                    if(i != j && matrix[i,j] == 0)
                    {
                        flag = false;
                    }
                }
            }
            if (flag == true)
            {
                Console.Write("YES");
            }
            else Console.Write("NO");

            Console.ReadKey();
        }
    }
}
