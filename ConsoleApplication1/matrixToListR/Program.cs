﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace matrixToListR
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int[,] matrix = new int[n, n];
            for (int i = 0; i < n; i++)
            {
                string s = Console.ReadLine();
                string[] s_arr = s.Split(' ');

                for (int j = 0; j < n; j++)
                {
                    matrix[i, j] = int.Parse(s_arr[j]);
                }
            }

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                   if (i<j && matrix[i,j] == 1)
                    {
                        Console.Write((i + 1) + " " + (j + 1));
                        Console.WriteLine();
                    }                    
                }
            }

            Console.ReadKey();
        }
    }
}
