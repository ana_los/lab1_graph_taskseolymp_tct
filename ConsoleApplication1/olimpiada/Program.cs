﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace olimpiada
{
    class Program
    {
        static void Main(string[] args)
        {
            string s = Console.ReadLine();
            string[] s_arr = s.Split(' ');

            int a = int.Parse(s_arr[0]);
            int b = int.Parse(s_arr[1]);
            int c = int.Parse(s_arr[2]);
            int d = int.Parse(s_arr[3]);

            int count = (b + c + d) - a;

            Console.Write(count);
            Console.ReadKey();
        }
    }
}
