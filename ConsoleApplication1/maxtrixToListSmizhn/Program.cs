﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace maxtrixToListSmizhn
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int[,] matrix = new int[n,n];
            for(int i = 0; i < n; i++)
            {
                string s = Console.ReadLine();
                string[] s_arr = s.Split(' ');

                for (int j = 0; j < n; j++)
                {
                    matrix[i, j] = int.Parse(s_arr[j]);
                }
            }

            
            string ind = "";
            int count = 0;
            for(int i = 0; i < n; i++)
            {
                ind = "";
                count = 0;
                for (int j = 0; j < n; j++)
                {
                    if (matrix[i,j] == 1)
                    {
                        ind += (j+1) + " ";
                        count++;
                    }
                }
                Console.Write((count) + " " + ind);
                Console.WriteLine();
            }

            Console.ReadKey();

        }
    }
}

