﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pirates
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string s = Console.ReadLine();
                string[] s_arr = s.Split(' ');
                int allmoney = int.Parse(s_arr[0]);
                int rest = int.Parse(s_arr[1]);
                int balance = allmoney - rest;

                int count_pirates = 0;
                int sum = 0, i = 1;
                while (sum != balance)
                {
                    sum += i;
                    count_pirates++;
                    i++;
                }
                Console.Write(count_pirates);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
            
            Console.ReadKey();
        }
    }
}
