﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gameStones
{
    class Program
    {
        static void Main(string[] args)
        {
            string s = Console.ReadLine();
            string[] s_arr = s.Split(' ');
            int n = int.Parse(s_arr[0]);
            int m = int.Parse(s_arr[1]);

            if (n % (m + 1) == 0)
            {
                Console.Write(0);
            }
            else Console.Write(n % (m + 1));

            Console.ReadKey();
        }
    }
}
