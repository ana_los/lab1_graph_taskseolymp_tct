﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {

            int[,] a = new int[100, 100];
            string s = Console.ReadLine();
            string[] s_arr = s.Split(' ');
            int countV =int.Parse( s_arr[0]);
            int countR = int.Parse(s_arr[1]);

            for (int i = 0; i < countR; i++)
            {
                s = Console.ReadLine();
                s_arr = s.Split(' ');
                int v1 = int.Parse(s_arr[0]);
                int v2 = int.Parse(s_arr[1]);
                a[v1 - 1, v2 - 1] = 1;
                a[v2 - 1, v1 - 1] = 1;
            }

            for (int i = 0; i < countV; i++)
            {
                for (int j = 0; j < countV; j++)
                {
                    Console.Write(a[i, j] + " ");
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}
