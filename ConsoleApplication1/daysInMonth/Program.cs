﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace daysInMonth
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int x = 0;
                int[] days = new int[] { 31,28,31,30,31,30,31,31,30,31,30,31 };
                string s = Console.ReadLine();
                string[] s_arr = s.Split(' ');
                int month = int.Parse(s_arr[0]);
                int year = int.Parse(s_arr[1]);

                if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
                {
                    days[1] += 1;
                    Console.Write(days[month-1]);
                }
                else Console.Write(days[month - 1]);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }

            Console.ReadKey();
        }
    }
}
