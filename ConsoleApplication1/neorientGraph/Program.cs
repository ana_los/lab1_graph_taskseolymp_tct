﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neorientGraph
{
    class Program
    {
        public static bool Check(int [,]matrix,int n,bool flag)
        {
            for (int i = 0; i < n; i++)
            {
                if (matrix[i,i]==1)
                {
                    flag = false; return flag;
                }
                else {
                    for (int j = 0; j < n; j++)
                    {
                        if (matrix[i, j] == 1 && matrix[j, i] != 1)
                        {
                            flag = false; return flag;
                        }
                    }
                }
            }
            return flag;
        }
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int[,] matrix = new int[n, n];
            for (int i = 0; i < n; i++)
            {
                string s = Console.ReadLine();
                string[] s_arr = s.Split(' ');

                for (int j = 0; j < n; j++)
                {
                    matrix[i, j] = int.Parse(s_arr[j]);
                }
            }

            bool flag = true;

            if (Check(matrix,n, flag) == true)
            {
                Console.Write("YES");
            }
            else Console.Write("NO");

            Console.ReadKey();
        }
    }
}
